import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import BottomTabs from './Navigation/tab';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { 
  IBMPlexSans_100Thin,
  IBMPlexSans_100Thin_Italic,
  IBMPlexSans_200ExtraLight,
  IBMPlexSans_200ExtraLight_Italic,
  IBMPlexSans_300Light,
  IBMPlexSans_300Light_Italic,
  IBMPlexSans_400Regular,
  IBMPlexSans_400Regular_Italic,
  IBMPlexSans_500Medium,
  IBMPlexSans_500Medium_Italic,
  IBMPlexSans_600SemiBold,
  IBMPlexSans_600SemiBold_Italic,
  IBMPlexSans_700Bold,
  IBMPlexSans_700Bold_Italic 
} from '@expo-google-fonts/ibm-plex-sans'
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import StackNavigation from './Navigation/StackNavigation';
import DetailsScreen from './screens/DetailsScreen';

const switchNavigator = createNativeStackNavigator();
export default function App() {
  
  const [fontsLoaded,setFontsLoaded] = useState(false);
  let [fontsLoad] = useFonts({
    IBMPlexSans_100Thin,
    IBMPlexSans_100Thin_Italic,
    IBMPlexSans_200ExtraLight,
    IBMPlexSans_200ExtraLight_Italic,
    IBMPlexSans_300Light,
    IBMPlexSans_300Light_Italic,
    IBMPlexSans_400Regular,
    IBMPlexSans_400Regular_Italic,
    IBMPlexSans_500Medium,
    IBMPlexSans_500Medium_Italic,
    IBMPlexSans_600SemiBold,
    IBMPlexSans_600SemiBold_Italic,
    IBMPlexSans_700Bold,
    IBMPlexSans_700Bold_Italic  
  })
  if(!fontsLoad){
    return (<AppLoading/>)
  }
      return (
      <NavigationContainer>
        <switchNavigator.Navigator screenOptions={{headerTitleAlign:'center'}}>
          <switchNavigator.Screen name='Main' options={{headerShown:false}} component={BottomTabs}/>
        </switchNavigator.Navigator>
      </NavigationContainer>
      ); 
};