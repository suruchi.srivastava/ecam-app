import React from 'react';
import { FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Row, Table } from 'react-native-table-component';
import Card from './Card.js';

const tableData = require('../Data.json')
function CreateTable(props,{navigation}) {
    const widthArr = [80, 80, 80, 101, 140, 80, 100, 80, 200,90,100,80,80,80,80,80,100];
    const heightArr = [40,40,40,40,40,40];
    const columns = ['Asset ID','Provider','Billing Account Name','Asset Account Name','Asset Type','Asset Name','Description','Category','Tags','Customer Total Cost','Dynamic Tags','org','spend','team','ibmc','awsdrg','publicCloud'];
    const data = [];
  
     tableData.map((item)=>{
        const dataRow = [];
          dataRow.push(' ');
          dataRow.push(item.providerName);
          dataRow.push(item.billingAccountName);
          dataRow.push(item.assetAccountName);
          dataRow.push(item.assetType);
          dataRow.push(item.assetName);
          dataRow.push(item.description);
          dataRow.push(item.category);
         let tag = '';
         if(item.allTags!=null){
            const tagData = Object.values(item.allTags);
            
            for (const [key, val] of Object.entries(tagData)) {
                if(tag == ''){
                    tag = Object.values(val);;
                }
                tag = tag + "\n" + Object.values(val);
            }
         }
         dataRow.push(tag);
         dataRow.push(item.cost + " " + item.corporateCurrency);
         dataRow.push("");
           dataRow.push(item.vattributes.org);
           dataRow.push("");
           dataRow.push("");
           dataRow.push("");
           dataRow.push("");
           dataRow.push("");
        data.push(dataRow);
     })
    return (
        <View style={styles.container}>
           <View style={{padding: 10,}}>
               <Text style={{fontSize:15,fontWeight:'bold' }}>Invoice Line Items:</Text>
           </View>
        {/* <ScrollView horizontal={true} > 
            <View style={{padding: 10}}>
            <Table borderStyle = {{borderWidth:1, borderColor:'black'}} style={{height:250}} >
                <Row data={columns} style={styles.HeadStyle} widthArr={widthArr} textStyle={styles.TableHeadText}></Row>
                <ScrollView >
                {
                  data.map((dataRow, index) => (
                    <Row   key={index} data={dataRow} widthArr={widthArr} borderStyle={{borderWidth:1,borderColor:'black'}}  style={styles.row} textStyle={styles.TableText}/> 
                    ))
                }
                </ScrollView>
            </Table>
            </View>
            {/* </ScrollView> */}
                <View style={{padding: 10, flexDirection:'row'}}>
                    <FlatList data={tableData} horizontal showsHorizontalScrollIndicator={false} keyExtractor={(item, index)=>item._id} renderItem={({item}) => (
                        <TouchableOpacity onPress={() =>alert(item._id)}>
                            <Card>
                                <View style={{padding:5, justifyContent:'space-between'}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{marginRight:10}}>Asset ID :</Text>
                                        <Text>{item._id}</Text>
                                    </View>
                                    <View style={{flexDirection:'row',paddingTop:5}}>
                                        <Text style={{marginRight:10}}>Asset Type :</Text>
                                        <Text>{item.assetType}</Text>
                                    </View>
                                    <View style={{flexDirection:'row',paddingTop:5}}>
                                        <Text style={{marginRight:10}}>Customer Cost :</Text>
                                        <Text>{item.ccCustomerTotalCost}</Text>
                                    </View>
                                    <View style={{flexDirection:'row',paddingTop:5}}>
                                        <Text style={{marginRight:10}}>Category :</Text>
                                        <Text>{item.category}</Text>
                                    </View>                 
                                </View>
                            </Card>
                        </TouchableOpacity>
                    )}/>
                </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor : '#ffffff',
    },
    row: { 
        height: 95, 
    },
    HeadStyle : {
        height:66,
        alignContent: 'center',
        backgroundColor : '#e6e3e3',
    },
    TableHeadText:{
        margin:8,
    },
    TableText:{
        margin:8,
        marginTop:-1,
    },
});

export default CreateTable;