import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import {View,FlatList,TouchableOpacity, Text} from 'react-native';
import Card from './Card';
var watchListData = require('../watchListData.json');
function InvoiceItem(props) {
    const navigation = useNavigation();
    const onPressBack =(params)=>{
        props.parentCallBack(params);
     }
     
    return (
        <View style={{ justifyContent:'center',alignContent:'center'}}>
        <FlatList  data={props.data}  horizontal showsHorizontalScrollIndicator={false} keyExtractor={(item, index) => item._id} renderItem={({item}) => (
            <TouchableOpacity onPress={()=>navigation.navigate('Details',{data:item,onPressBack})}>
                <Card color={props.color}>
                    <View style={{ justifyContent:'space-between',padding:7}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{marginRight:10, fontFamily:'IBMPlexSans_400Regular'}}>Asset ID :</Text>
                            <Text style={{fontFamily:'IBMPlexSans_400Regular'}}>{item._id}</Text>
                        </View>
                        {props.provider ? null : <View style={{flexDirection:'row',paddingTop:5}}>
                            <Text style={{marginRight:10,fontFamily:'IBMPlexSans_400Regular'}}>Asset Type :</Text>
                            <Text style={{fontFamily:'IBMPlexSans_400Regular'}}>{item.assetType}</Text>
                        </View> }
                        
                        <View style={{flexDirection:'row',paddingTop:5,marginBottom:5}}>
                            <Text style={{marginRight:10,fontFamily:'IBMPlexSans_400Regular'}}>Category :</Text>
                            <Text style={{fontFamily:'IBMPlexSans_400Regular'}}>{item.category}</Text>
                        </View> 

                        {props.provider ?  <View style={{flexDirection:'row',marginBottom:5}}>
                            <Text style={{marginRight:10,fontFamily:'IBMPlexSans_400Regular'}}>Provider :</Text>
                            <Text style={{fontFamily:'IBMPlexSans_400Regular'}}>{item.providerName}</Text>
                        </View> : null }
                        {props.provider ? null :
                            <View style={{flexDirection:'row',justifyContent:'center',borderColor:'black',borderWidth:1,borderRadius:5, marginTop:5}}>
                                <Text style={{marginRight:10,fontFamily:'IBMPlexSans_400Regular'}}>$ :</Text>
                                <Text style={{fontFamily:'IBMPlexSans_400Regular'}}>{item.ccCustomerTotalCost}</Text>
                            </View>
                    }
                    </View>
                </Card>
            </TouchableOpacity>
        )}/>
    </View> 
    );
}

export default InvoiceItem;