import React from 'react';
import {StyleSheet, View} from 'react-native';

function Card(props) {
    
    return (
        <View style={[styles.Card,{backgroundColor:props.color}]}>
            <View style={styles.CardContent}>

                {props.children}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Card:{
       // backgroundColor: "#CCFFFF",
     //  backgroundColor:"#F8CECC",
        borderWidth:0.9,
        marginHorizontal:4,  
        borderRadius: 15,
        marginBottom: 3,
        marginRight:10,
        shadowColor: "#000000",
        shadowOffset: {
            width: 10,
            height: 20,
        },
        shadowOpacity: 0.3,
        shadowRadius: 60,
        elevation: 15,
    },
    CardContent:{
        marginHorizontal:10,
        marginVertical:2,
    },
})
export default Card;