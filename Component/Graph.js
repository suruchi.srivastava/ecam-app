import React, { useState } from 'react'
import { View, Text,StyleSheet, Dimensions, Image } from 'react-native'
import { LineChart } from 'react-native-chart-kit'
import { Rect, Text as TextSVG, Svg } from "react-native-svg";

const Charts = (props) => {
    let [tooltipPos, setTooltipPos] = useState({ x: 0, y: 0, visible: false, value: 0 })

    return (
        <>
        <View  style={styles.container}>
        <Text style={styles.textAsset}>Cost</Text>
        <View>
            <LineChart  data={props.datasets}
           withHorizontalLabels={false}
            width={Dimensions.get('window').width -50}
                height={200}
                yAxisLabel="$"
                yAxisSuffix="k"
                yAxisInterval={1}
                chartConfig={{
                    backgroundColor: '#fcfafa',
                    backgroundGradientFrom: '#fcfafa',
                    backgroundGradientTo: '#fcfafa',
                    decimalPlaces: 2,
                    fillShadowGradient: "#699BF7",
              fillShadowGradientOpacity: 0.5, 
              color: (opacity = 1) => `rgba(63, 143, 244, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                    style: {
                        borderRadius: 0
                    },
                    propsForDots: {
                        r: "5",
                        strokeWidth: "0",
                        stroke: "#fbfbfb"
                    }
                }}
                bezier
                style={{
                    marginVertical: 8,
                    borderRadius: 6,
                    marginTop: 20
                }}

                decorator={() => {
                    return tooltipPos.visible ? <View>
                        <Svg>
                            <Rect x={tooltipPos.x - 15} 
                                y={tooltipPos.y + 10} 
                                width="90" 
                                height="40"
                                fill="#699BF7" />
                                <TextSVG
                                    x={tooltipPos.x + 30}
                                    y={tooltipPos.y + 30}
                                    fill="white"
                                    fontSize="12"
                                    fontWeight="bold"
                                    textAnchor="middle">
                                    {tooltipPos.value}
                                </TextSVG>
                        </Svg>
                    </View> : null
                }}

                onDataPointClick={(data) => {

                    let isSamePoint = (tooltipPos.x === data.x 
                                        && tooltipPos.y === data.y)

                    isSamePoint ? setTooltipPos((previousState) => {
                        return { 
                                  ...previousState,
                                  value: data.value,
                                  visible: !previousState.visible
                               }
                    })
                        : 
                    setTooltipPos({ x: data.x, value: data.value, y: data.y, visible: true });

                }}
            />
        </View>
        </View>
        </>
    )
}
const styles = StyleSheet.create({
    container: {
      marginHorizontal: "2.5%",
      marginBottom: 5,
      width: '95%',
      marginTop: 10,
      padding: 10,
      fontSize: 30,
      fontWeight: "900",
      borderRadius: 20,
      borderWidth: 1,
      borderColor: '#dbb8cd',
      shadowColor: '#fcfafa',
      shadowOpacity: 0.1,
      elevation: 1,
    },
    textAsset:{
      fontSize: 20,
      fontWeight: 'bold',
      marginBottom:10,
      textAlign:'center',
      fontFamily:"IBMPlexSans_600SemiBold"
      
      
    },
    downloadicon: {
        maxHeight: 30,
        maxWidth: 30,
        tintColor: "#699BF7",
        flexDirection: "row",
        position: "absolute",
        marginLeft: "60%",
      },
      downloadiconview: {
        fontSize: 16,
        paddingVertical: 8,
        paddingHorizontal: 20,
        borderRadius: 45,
      },
      graphselectorview: {
        fontSize: 16,
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 13,
        borderRadius: 45,
        backgroundColor: "#CCFFFF",
      },
      graphselector: {
        fontSize:17,
        color: "black",
        fontFamily:"IBMPlexSans_600SemiBold",
      },
});

export default Charts
