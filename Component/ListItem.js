import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import InvoiceItem from './InvoiceItem.js';

const tableData = require('../Data.json')
function LineItem(props,{navigation}) {
    const[data,setData] = useState([]);
    const [showAmazonItem,setShowAmazon] = useState(false);
    const [showIBMItem, setShowIBMItem] = useState(false);
    const [showGCPItem, setShowGCPItem] = useState(false);
    const [showAzurItem, setShowAzurItem] = useState(false);
    const amazonInvoice = [];
    const ibmInvoice = [];
    const gcpInvoice = [];
    const azureInvoice = [];
    var gcpAverage = 0;
    var amazonAverage = 0;
    var ibmAverage = 0;
    var azureAverage = 0;

    const handleCallBack = (param)=>{
        setData(param);
    }

    tableData.map((item)=>{
        if(item.providerName == 'gcp'){
            gcpInvoice.push(item);
            gcpAverage +=  item.cost;
        }
        else if(item.providerName == 'amazon'){
            amazonInvoice.push(item);
            amazonAverage +=  item.cost;
        }
        else if(item.providerName == 'ibm'){
            ibmInvoice.push(item);
            ibmAverage +=  item.cost;
        }
        else {
            azureInvoice.push(item);
            azureAverage +=  item.cost;
        }
    })
    
    amazonAverage = amazonAverage.toFixed(2);
    ibmAverage = ibmAverage.toFixed(2);
    gcpAverage = gcpAverage.toFixed(2);
    azureAverage = azureAverage.toFixed(2);

    var amazonBarWidth,azureBarWidth,ibmBarWidth,gcpBarWidth;

    (Math.round(amazonAverage/2)==0) ? (amazonBarWidth = (amazonAverage *1000)+50):(amazonAverage < 250 ? (amazonBarWidth = amazonAverage) : amazonBarWidth = 280);
    (Math.round(azureAverage/2)==0) ? (azureBarWidth = azureAverage *1000 ):(azureAverage < 250 ? (azureBarWidth = azureAverage) : azureBarWidth = 280);
    (Math.round(ibmAverage/2)==0) ? (ibmBarWidth = (ibmAverage *1000 )+ 50):(ibmAverage < 250 ? (ibmBarWidth = ibmAverage) : ibmBarWidth = 280);
    (Math.round(gcpAverage/2)==0) ? (gcpBarWidth = (gcpAverage *1000)+ 50):(gcpAverage < 250 ? (gcpBarWidth = gcpAverage) : gcpBarWidth = 280);

    function renderTitleSection(invoiceTitle){
        return (
            <View style={{padding: 8, flexDirection:'row',justifyContent:'space-between' }}>
                <Text style={{fontSize:23,fontFamily:'IBMPlexSans_600SemiBold' }}>{invoiceTitle}</Text>
            </View>
        );
    }
    function renderProgressSection(barColor,barwidth, cost){
        return(
            <View style={{flexDirection:'row', marginLeft:8}}>
               <ProgressBar style={[styles.progressBarStyle,{width:barwidth}]} progress={1}  color={barColor} />
                <Text style={{marginLeft:17,fontFamily:'IBMPlexSans_400Regular'}}>{cost} $</Text>
            </View>
        );
    }
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={{marginLeft:15}}>
                    {renderTitleSection('Amazon')}
                    <TouchableOpacity onPress={() =>{setShowAmazon(!showAmazonItem)}}>
                    {renderProgressSection('#6E32C9',amazonBarWidth,amazonAverage)}
                    </TouchableOpacity>
                   {showAmazonItem ? <InvoiceItem data={amazonInvoice} provider={false} color={'#ffffff'} parentCallBack={handleCallBack} />:null}
                </View>
                <View style={{marginLeft:15}}>
                    {renderTitleSection('IBM')}
                    <TouchableOpacity onPress={() =>{setShowIBMItem(!showIBMItem)}}>
                      
                        {renderProgressSection('#1191F0',ibmBarWidth,ibmAverage)}
                    </TouchableOpacity>
                    {showIBMItem ?<InvoiceItem data={ibmInvoice} provider={false} color={'#ffffff'} parentCallBack={handleCallBack}/>:null}
                </View>
                <View style={{marginLeft:15}}>
                    {renderTitleSection('GCP')}
                    <TouchableOpacity onPress={() =>{setShowGCPItem(!showGCPItem)}}>
                        {renderProgressSection('#006161',gcpBarWidth,gcpAverage)}
                    </TouchableOpacity>
                    {showGCPItem ?<InvoiceItem data={gcpInvoice} provider={false} color={'#ffffff'} parentCallBack={handleCallBack}/>:null}
                </View>
                <View style={{marginLeft:15}}>
                    {renderTitleSection('Azure')}
                    <TouchableOpacity onPress={() =>{setShowAzurItem(!showAzurItem)}}>
                        {renderProgressSection('#A11950',azureBarWidth,azureAverage)}
                    </TouchableOpacity>
                    {showAzurItem ?<InvoiceItem data={azureInvoice} provider={false} color={'#ffffff'} parentCallBack={handleCallBack}/>:null}
                </View>
                <View>
                    <Text>
                    </Text>
                </View>
            </ScrollView>   
            
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        backgroundColor : '#ffffff',
        height: "80%",
    },
    progressBarStyle:{
        borderWidth:1.8,
        shadowColor: "black",
        elevation: 3,
        marginBottom:10,
       // marginLeft:20, 
        height:19,
        borderRadius:5,
    },
});
export default LineItem;