import React from 'react';
import { View, StyleSheet, Text,SafeAreaView } from 'react-native';

function Header(props) {
    return (
        <View  style={styles.header}>
            <View style={{marginLeft:10}}>
                <Text style={styles.headerText}>Kyndryl</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header:{
        width:'100%',
        height:'6%',
        justifyContent:'center',
        backgroundColor:'black'
    },
    headerText:{
        fontWeight:'bold',
        fontSize:20,
        color:'red'
    }
})

export default Header;