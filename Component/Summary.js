import { StyleSheet, Text, View, Image } from "react-native";

const date = new Date();
const month = date.getMonth();

const costdataset = [
  1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000,
];
const assetsdataset = [
  120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10
];

function getpmcost(monthnumber) {
  var cost = costdataset;
  return cost[monthnumber];
}
const ppmcost = getpmcost(month - 1);
const pmcost = getpmcost(month);

var costarrowurl = "";
var costarrowtint = "";
if (pmcost >= ppmcost) {
  costarrowurl = require("../assets/arrow-up.png");
  costarrowtint = "black";
}
else {
  costarrowurl = require("../assets/arrow-down.png");
  costarrowtint = "black";
}

function getpmassets(monthnumber) {
  var assets = assetsdataset;
  return assets[monthnumber];
}
const ppmassets = getpmassets(month - 1);
const pmassets = getpmassets(month);

var assetsarrowurl = "";
var assetsarrowtint = "";
if (pmassets >= ppmassets) {
  assetsarrowurl = require("../assets/arrow-up.png");
  assetsarrowtint = "black";
}
else {
  assetsarrowurl = require("../assets/arrow-down.png");
  assetsarrowtint = "black";
}

export function Summary(props) {
  return (
    <>
    <View style={styles.container}>
      <View style={{flexDirection:"row"}}>
        <Text style={styles.title}>{props.pageTitle}</Text>
        <View style={styles.downloadiconview}><Image source={require("../assets/download.png")} style={styles.downloadicon}/></View>
       
      </View>
      <View style={styles.summaryview}>
        <Text style={styles.summarytext}>
          <Text>
            <Image
              source={costarrowurl}
              style={styles.summarycosticon}
              resizeMode="contain"
            />
          </Text>
          <Text style={{fontFamily:'IBMPlexSans_300Light'}}
          >
            {" "} Cost : {pmcost} $ {" "}
          </Text>
          <Text>
            <Image
              source={assetsarrowurl}
              style={styles.summaryassetsicon}
              resizeMode="contain"
            />
          </Text>
          <Text style={{fontFamily:'IBMPlexSans_300Light'}}
          >
            {" "} Assets : {pmassets} {" "}
          </Text>
        </Text>
      </View>
      
    </View>
    <View style={{flexDirection: "row", justifyContent: "space-evenly", marginTop:-10}}>
    <View style={styles.graphselectorview}><Text style={styles.graphselector}>AWS</Text></View>
    <View style={styles.graphselectorview}><Text style={styles.graphselector}>GCP</Text></View>
    <View style={styles.graphselectorview}><Text style={styles.graphselector}>IBM</Text></View>
    <View style={styles.graphselectorview}><Text style={styles.graphselector}>Azure</Text></View>
  </View>
  </>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  title: {
    color: "#000000",
    fontSize: 26,
    fontFamily:'IBMPlexSans_600SemiBold',
    textAlign: "center",
    marginBottom: 10,
  },
  summarytext: {
    color: "black",
    fontSize: 20,
    textAlign: "center",
  },
  downloadicon: {
    maxHeight: 30,
    maxWidth: 30,
    tintColor: "#000000",
    flexDirection: "row",
    position: "absolute",
    marginLeft: "60%",
  },
  downloadiconview: {
    fontSize: 16,
    marginLeft:10,
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 45,
  },
  summarycosticon: {
    width: 16,
    height: 16,
    borderRadius: 0,
    alignSelf: "center",
    tintColor: costarrowtint,
  },
  summaryassetsicon: {
    width: 16,
    height: 16,
    borderRadius: 0,
    alignSelf: "center",
    tintColor: assetsarrowtint,
  },
  summaryview: {
    fontSize: 20,
    fontWeight: "300",
   // padding: 10,
    width: "100%",
    backgroundColor: "#ffffff",
    marginBottom: 20,
  },
  summarybox: {
    //borderWidth: 1,
    fontSize: 20,
    fontWeight: "300",
    padding: 10,
    width: "100%",
    backgroundColor: "#F1F1F1",
    borderRadius: 25,
    marginBottom: 40,
    shadowColor: "black",
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.2,
    shadowRadius: 60,
    elevation: 10,
  },
  graphselectorview: {
    fontSize: 16,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 13,
    borderRadius: 45,
    backgroundColor: "#CCFFFF",
  },
  graphselector: {
    fontSize:17,
    color: "black",
    fontFamily:"IBMPlexSans_600SemiBold",
  },
});
