import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { BarChart } from "react-native-gifted-charts";

const App = (props) =>  {

    return (
      <>
        <View style={styles.container}>
          <Text style={styles.textAsset}>Asset by Count</Text>
          <BarChart
            showFractionalValue
            showYAxisIndices
            barBorderRadius={4}
            noOfSections={5}
            maxValue={300}
            data={props.dataValues}
            spacing={30}
            barWidth={35}
            isAnimated
          />
        </View>
      </>
    );

  

}
const styles = StyleSheet.create({
  container: {
    marginHorizontal: "2.5%",
   // marginBottom: 10,
    width: '95%',
    padding: 10,
    marginTop: 10,
    fontSize: 30,
    fontWeight: "900",
    borderRadius: 20,
    marginLeft: 10,
    borderWidth: 1,
    shadowColor: '#fcfafa',
    borderColor: '#dbb8cd',

    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowOpacity: 0.1,
    shadowRadius: 500,
    elevation: 1,
  },
  textAsset: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
    fontFamily: "IBMPlexSans_600SemiBold"
  },
  graphselectorview: {
    fontSize: 16,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 13,
    borderRadius: 45,
    backgroundColor: "#CCFFFF",
  },
  graphselector: {
    fontSize: 17,
    color: "black",
    fontFamily: "IBMPlexSans_600SemiBold",
  },
});
export default App