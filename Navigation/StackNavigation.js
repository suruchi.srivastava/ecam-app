import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import AssetScreen from '../screens/AssetScreen';
import CostScreen from '../screens/CostScreen';
import DetailsScreen from '../screens/DetailsScreen';
import HomeScreen from '../screens/HomeScreen';
const Stack = createNativeStackNavigator();

function Homenavigation(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerShown:false}} name='homeScreen' component={HomeScreen}/>
            <Stack.Screen options={{headerShown:false}} name='Details' component={DetailsScreen}/>
        </Stack.Navigator>
    );
}

export {Homenavigation};
function Costnavigation(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerShown:false}} name='costs' component={CostScreen}/>
            <Stack.Screen options={{headerShown:false}} name='Details' component={DetailsScreen}/>
        </Stack.Navigator>
    );
}

export {Costnavigation};
function Assetnavigation(props) {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerShown:false}} name='assets' component={AssetScreen}/>
            <Stack.Screen options={{headerShown:false}} name='Details' component={DetailsScreen}/>
        </Stack.Navigator>
    );
}

export {Assetnavigation};
