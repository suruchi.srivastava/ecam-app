import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import  Icon  from 'react-native-vector-icons/FontAwesome5';
import {Homenavigation,Costnavigation, Assetnavigation} from './StackNavigation';
const Tab = createBottomTabNavigator();

function Tabs(props) {
    return (
        <Tab.Navigator 
            screenOptions={({ route }) => ({
                headerShown:false,
                tabBarStyle: {
                    position: 'absolute',
                    bottom:10,
                    left:20,
                    right:20,
                    elevation:0,
                    backgroundColor: '#ffffff',
                    borderRadius: 30,
                    height:60,
                    shadowColor: '#000000',
                    shadowOffset: {
                        width:0,
                        height:20,
                    },
                    shadowOpacity:0.75,
                    shadowRadius:3.5,
                    elevation:5,
                },
                tabBarShowLabel:false,
                tabBarIcon : ({ focused, color }) => {
                    let iconName;
                    if(route.name =='Home'){
                        iconName = focused ? 'home' : 'home';
                    }
                    else if(route.name == 'Costs'){
                        iconName = focused ? 'file-invoice-dollar' : 'file-invoice-dollar';
                    }
                    else if(route.name == 'Assets'){
                        iconName = focused ? 'chart-bar' : 'chart-bar';
                    }
                    return <Icon name={iconName} size={35} color={color} />;
                },
                tabBarActiveTintColor: '#669BF7',
                tabBarInactiveTintColor: "grey",
            })}
        >
           <Tab.Screen name='Home' component={Homenavigation}/>
           <Tab.Screen name='Costs' component={Costnavigation}/> 
           <Tab.Screen name='Assets' component={Assetnavigation}/> 
        </Tab.Navigator>
    );
}

export default Tabs;