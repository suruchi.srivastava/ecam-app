import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Summary } from '../Component/Summary';
import Graph from '../Component/Graph';
import LineItem from '../Component/ListItem';

function CostScreen(props) {
    return (
        <View style={styles.container}>
            <View>
                <Summary pageTitle={'Cost Dashboard'}/>
            </View>
            <View>
                <Graph  datasets={{
              labels: [
                "Nov'21",
                "Dec'21",
                "Jan'22",
                "Feb'22",
               
              ],
              datasets: [
                {
                  data: [165931.475375, 247585.091090, 233829.364440, 240593.093792 ],
                  strokeWidth: 2,
                  color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
                  
                },
              ],
            }}/>
             </View>
            <View style={{flex:1}}>
                <LineItem/> 
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
       // marginTop:'10%',
        backgroundColor : '#ffffff',
    },
})

export default CostScreen;