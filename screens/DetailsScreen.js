import React, { useEffect, useState } from 'react';
import { View,Text, TouchableOpacity,StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Graph from '../Component/Graph';


var watchListData = require('../watchListData.json');
function DetailsScreen({route,navigation}) {
    const[data,setData] = useState([]);
   var color;
   if(route.params.data.utilization == "High"){
       color = "#7cc7ff";
   }
   else if(route.params.data.utilization == "Medium"){
    color = "#8863a9";
   }
   else if(route.params.data.utilization == "Low"){
    color = "#e279ac";
   }
   else {
    color = "#a41523";
   }
  
    return (
       <View style={styles.container}>

            <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:5}}>
               <TouchableOpacity>
                    <Icon name='arrow-left' color={'#000000'} size={30} onPress={()=>{route.params.onPressBack(data); navigation.goBack();}}/>
                </TouchableOpacity>
                <TouchableOpacity style={{marginRight:10}} onPress={() => {watchListData.push(route.params.data); }}>
                <Icon name='plus-circle' color={'#000000'}  size={35} />
                </TouchableOpacity>
                
            </View>
            <View style={{marginLeft:5,flexDirection:'row'}}>
                <Text style={styles.textFont}>Asset Name:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data.assetAccountName}</Text> 
            </View>
            <View style={{marginLeft:-10,marginRight:-10}}>
                <Graph  datasets={{
              labels: [
                "Nov'21",
                "Dec'21",
                "Jan'22",
                "Feb'22",
               
              ],
              datasets: [
                {
                  data: [165980.475375, 212345.091090, 263748.364440,  133829.364440, 240593.093792 ],
                  strokeWidth: 2,
                  color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
                  
                },
              ],
            }} />
            </View>
            <View style={{borderWidth:0.5,padding:15,borderRadius:20,justifyContent:'center',marginTop:'2%'}}>
           
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Asset ID:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data._id}</Text> 
            </View>
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Asset Type:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data.assetType}</Text> 
            </View>
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Utilization:</Text>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:15,height:15,marginLeft:20, borderWidth:1,borderColor:color, backgroundColor:color,marginTop:6}}></View>
                    <Text style={[styles.textFont,{marginLeft:12}]}>{route.params.data.utilization}</Text> 
                </View>
                
            </View>
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Provider:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data.providerName}</Text> 
            </View>
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Cost:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data.ccCustomerTotalCost} $</Text> 
            </View>
            <View style={styles.textStyle}>
                <Text style={styles.textFont}>Description:</Text>
                <Text style={[styles.textFont,{marginLeft:20}]}>{route.params.data.description}</Text> 
            </View>
            
            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:'4%',
        marginTop:'5%',
        backgroundColor:'white'
    },
    textStyle:{
        flexDirection:'row',
        marginVertical:8,
    },
    textFont:{
        fontSize:17,
        fontFamily:'IBMPlexSans_300Light',
    }
})
export default DetailsScreen;