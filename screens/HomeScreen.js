import React, { useEffect, useState,  } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { ScrollView } from 'react-native';
import Card from '../Component/Card';
import InvoiceItem from '../Component/InvoiceItem';

const tableData = require('../Data.json')
var watchListData = require('../watchListData.json');
function HomeScreen({navigation}) {
  const[data,setdata] = useState([]);
  watchListData = require('../watchListData.json');
    useEffect(() => {
       
         navigation.addListener('focus', async () => {
           
        });
    
      });
      const handleCallBack = (param)=>{
          setdata(param);
      }
    return (
        
        <View style={styles.container}>
            <View style={{margin:20,alignItems:'center'}}>
                <View style={{flexDirection:'row'}}>
                    <Image source={require('../assets/Kyndryl_logo.svg.png')}  style={{width:'40%', height:'90%', resizeMode: 'contain', marginTop:5}} />
                    <Text style={{fontSize:30, fontFamily:'IBMPlexSans_600SemiBold'}}>ECAM</Text>
                </View>
                
                <ScrollView scrollEnabled={false}>
                <View style={{alignSelf:'baseline', flexDirection:'row',justifyContent:'space-between', marginTop:15}}>
                    <Text style={{ marginRight:10, fontSize:22, fontFamily:'IBMPlexSans_600SemiBold'}}>Explore</Text>
                </View>
                
                    <View style={{flex:1,marginTop:10, flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Costs')}>
                            <Card color={"#ffffff"}>
                                <Text style={{fontSize:30,padding:15,marginLeft:20,marginRight:20, alignSelf:'center',fontFamily:'IBMPlexSans_600SemiBold'}}>Cost</Text>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginLeft:'4%'}}  onPress={()=>navigation.navigate('Assets')}>
                            <Card color={"#ffffff"}>
                            <Text style={{fontSize:30,padding:15, marginLeft:10,marginRight:10,alignSelf:'center',fontFamily:'IBMPlexSans_600SemiBold'}}>Assets</Text>
                            </Card>
                        </TouchableOpacity>
                        
                    </View>
                    <View>
                        <View style={styles.dataContainer}>
                            <Text style={styles.textFont}>Top Underutilized</Text>
                        </View>
                        <View style={{marginTop:10}}>
                            <InvoiceItem data={tableData} provider={true} color={"#CCFFFF"}  parentCallBack={handleCallBack}/>
                        </View>
                    </View>

                    <View>
                        <View style={styles.dataContainer}>
                            <Text style={styles.textFont}>Top Cost Causing</Text>
                            
                        </View>
                        <View style={{marginTop:10}}>
                            <InvoiceItem data={tableData} provider={true} color={"#F8CECC"} parentCallBack={handleCallBack}/>
                        </View>
                    </View>
                    
                    <View>
                        <View style={styles.dataContainer}>
                            <Text style={styles.textFont}>WatchList</Text>
                            
                        </View>
                        
                        <View style={{marginTop:2,flex:1, height:150 }}>
                            <ScrollView>
                                
                                { watchListData.map((item,index)=>{
                                    var costUrl =require("../assets/arrow-up.png");
                                   var data = 0;
                                   (item.ccCustomerTotalCost > data) ? costUrl=require("../assets/arrow-up.png"): costUrl=require("../assets/arrow-down.png");
                                    return (
                                        <View key={index}>
                                    <View style={{flexDirection:'row',padding:3}} >
                                        <Text style={{fontSize:15, fontFamily:'IBMPlexSans_400Regular'}} >Asset ID :</Text>
                                        <Text style={{fontSize:15, fontFamily:'IBMPlexSans_400Regular'}}>{item._id}</Text>
                                        <Image
                                            source={costUrl}
                                            style={styles.summaryassetsicon}
                                            resizeMode="contain"
                                            />
                                        <Text >{item.ccCustomerTotalCost.toFixed(2)} $</Text>
                                    </View>
                                    <View style={{borderWidth:1, borderStyle:'dashed', borderRadius:1,borderColor:'black'}}></View>  
                                    </View>
                                    );
                                    data = item.ccCustomerTotalCost;
                                    console.alert(data);
                                })}
                            </ScrollView>
                            
                        </View>
                    </View>
                
                </ScrollView>
                
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        flex:1,
       // marginTop:'10%',
    },
    dataContainer:{
        alignSelf:'baseline', 
        justifyContent:'space-between', 
        marginTop:20,
    },
    textFont:{
        fontFamily:'IBMPlexSans_600SemiBold',
        marginRight:10, 
        fontSize:22, 
    },
    summaryassetsicon: {
        marginLeft:15,
        width: 16,
        height: 16,
        borderRadius: 0,
        alignSelf: "center",
        tintColor: 'black',
      }
})
export default HomeScreen;