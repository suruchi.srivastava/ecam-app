import React from 'react';
import { View,StyleSheet, Image, SafeAreaView } from 'react-native';

function ProfileScreen({navigation}) {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.imageContainer}>
                    <View style={styles.profileImage}>
                        <Image source={require('../assets/profile-icon.png')} style={styles.image} resizeMode='center'/>
                    </View>    
            </View>       
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
    },
    imageContainer: {
        alignSelf:'center',
        marginTop:-80,
    },
    image:{
        flex:1,
        width:undefined,
        height:undefined,
    },
    profileImage:{
        width:200,
        height:200,
        borderRadius:100,
        overflow:'hidden',
    },
})

export default ProfileScreen;