import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { Summary } from '../Component/Summary';
import Bar from '../Component/Barchart';
import LineItem from '../Component/ListItem';

function AssetScreen() {
    return (
        <View style={styles.container}>
            <View>
                <Summary pageTitle={'Assets Dashboard'} />
            </View>
            <View>
                <Bar dataValues={
                    [
                    {
                        value: 130, label: "Jan",
                        topLabelComponent: () => (
                            <Text style={{ color: 'black', fontSize: 15, marginBottom: 6 }}>100</Text>
                        ),
                        frontColor: '#4ABFF4'
                    },
                    {
                        value: 180, label: "Feb",
                        frontColor: '#79C3DB',
                        topLabelComponent: () => (
                            <Text style={{ color: 'black', fontSize: 15, marginBottom: 6 }}>180</Text>
                        ),
                    },
                    {
                        value: 250, label: "Mar",
                        topLabelComponent: () => (
                            <Text style={{ color: 'black', fontSize: 15, marginBottom: 6 }}>250</Text>
                        ),
                        frontColor: '#28B2B3'
                    },
                    {
                        value: 60, label: "Apr",
                        topLabelComponent: () => (
                            <Text style={{ color: 'black', fontSize: 15, marginBottom: 6 }}>60</Text>
                        ),
                        frontColor: '#4ADDBA'
                    },
    ]
                } />
            </View>
            <View style={{ flex: 1 }}>
                <LineItem />
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //  marginTop:'10%',
        backgroundColor: '#ffffff',
    },
})
export default AssetScreen;